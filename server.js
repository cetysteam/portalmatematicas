//Load modules
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = require('./config/db');
var path = require('path');
var passport = require('passport');
var morgan = require('morgan');

app.use(bodyParser.json());
app.use(bodyParser.json({
  type: 'application/vnd.api+json'
}));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(express.static(__dirname));
app.use(express.static(__dirname + 'public/'));
//Make server CORS-ENABLE
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(passport.initialize());

mongoose.connect(db.url);

//Load routes
require('./app/routes')(app);

//Start app
app.listen(8080, function() {
  console.log('Listening on port ' + 8080);
});


exports = module.exports = app;
