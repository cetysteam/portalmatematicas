myApp.controller('registerController', function ($scope, $location, $http) {
    $http.post('/getCountries',{test:"f"}).then(function(respond){
      $scope.countries = respond.data;
      console.log(respond);
    },function(err){

      console.log(err);
    });

    $scope.estados = "";
    $scope.checkCountry = () => {
      if( $scope.Pais == "Mexico" ){
        //Agrega opcion de estados
        if( $scope.estados == "" ){
          $http.get('/getEstados').then(
            function(respond){
              $scope.estados = respond.data;
            }, function(err){
              console.log(err);
            }
          );
        }

        $('#estados').fadeIn('slow');
      }else{
        $('#estados').hide('slow');
      }
    }

  

    $scope.changeview = function (view) {
        $location.path(view);
    }
    $scope.sendData = (view) => {
        var email = $scope.email;
        var password = $scope.password;
        var name = $scope.Nombre;
        var apellidoP = $scope.Apellido_P;
        var apellidoM = $scope.Apellido_M;
        var pais = $scope.Pais;
        var estado = $scope.Estado;
        var role = "alumno";
        if($scope.rol === true){
            role = "maestro";
        }
        // else{

        // }

        $scope.body = {
            email: email,
            password: password,
            Nombre: name,
            Apellido_P: apellidoP,
            Apellido_M: apellidoM,
            Escuela: $scope.Escuela,
            Pais: pais,
            Estado: estado,
            role: role
        }
        // console.log(role);
        if (email != "" || password != "" || name != "" || apellidoP != "") {
            $http.post('/signup', $scope.body).
                then(function (success) {
                    $location.path(view);
                }, function (error) {
                    console.log(error);
                });
        }
        else {
            alert("no se han llenado todos los campos.");
        }
    }
});
