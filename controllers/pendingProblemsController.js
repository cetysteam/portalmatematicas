myApp.controller('pendingProblemsController', function ($scope, $http, $location,$rootScope) {
    $scope.problems = [];

    var renderLatex = ()=>{
        $('tbody').ready(function () {
            MathJax.Hub.Queue(["Typeset",MathJax.Hub, "latex"]);
        });
    }

    $scope.page = 1;
    // localStorage.clear();
    var token = localStorage.getItem('token');

    $http.get('/api/getAllAnswers',{headers: {authorization: token}}).then(function(response){
      var lenUsers = response.data.length;
      console.log(lenUsers);
      var problems = [];
      console.log(response);
      for(var i = 0; i < lenUsers; i++){
        var lenProblemUsers = response.data[i].answers.length;
        var name = response.data[i].nombre;
        var userName = name;

        for(var n = 0; n < lenProblemUsers; n++){
          //problems.push({
          //  nombre = name;
            //problemId = response.data[i].answers[n].problem_id;
            //respuesta = response.data[i].answers[n].answer;
          //});
        }
//
        console.log(lenProblemUsers);
        //console.log(problems);
      }

    });



    $http.get('/api/problems?page=' + $scope.page, { headers: { authorization: token } })
        .then(function (response){
            $scope.problems = response.data;
            renderLatex();

            // console.log(response.data);
        },function (error){
            if(error.data === "Unauthorized"){
                $location.path('/');
            }
            //console.log(error);
        }
    );
    $scope.changePage = (direction) => {
        if (direction == 'next'){
            $scope.page += 1;

        }
        else if (direction == 'previous'){
            $scope.page -= 1;

        }
        $http.get('/api/problems?page=' + $scope.page, { headers: { authorization: token } })
            .then(function (response) {
                $scope.problems = response.data;
                renderLatex();
            }, function (error) {
                console.log(error);
                if(error.data === "Unauthorized"){
                    $location.path('/');
                }
            }  );

        if($scope.page > 1){
            $("#previous-button").removeClass("disabled");
            console.log("Next");
        }
        else{
            $("#previous-button").addClass("disabled");
        }
    };
    $scope.clicked = function (_id) {
        // localStorage.setItem('data', _id);
        // $rootScope.problemId = _id;
        sessionStorage.setItem('problemId',_id);
        $location.path('problem');
    };
});
