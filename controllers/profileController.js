myApp.controller('profileController', function($scope, $location, $http, $window){

    let token = localStorage.getItem('token');
    // $scope.user = {};

    $http.get('/getProfile',  { headers: { authorization: token }}).then(function (response){
        console.log(response);
        $scope.user = response.data;


    }, function (error){
        if(error.data === "Unauthorized"){
            $location.path('/');
        }
        console.log(error);
    });


        var renderLatex = ()=>{
            $('.latex').ready(function () {
                MathJax.Hub.Queue(["Typeset",MathJax.Hub, "latex"]);
            });
        }
    $http.get('/api/getAnswers', { headers: { authorization: token } })
    .then(function (response){
     console.log(response);
     var arr = [];
     var len = response.data.length;

     for (var i = 0; i < len; i++) {
        var tempStatus = response.data[i].status;
        if (tempStatus = "Pending"){
          tempStatus = "Pendiente de revisar";
        }
        arr.push({
          area : response.data[i].problem.area,
          tema : response.data[i].problem.tema,
          problema : response.data[i].problem.pregunta,
          dificultad : response.data[i].problem.nivel,
          estado : tempStatus
        });
      }

      $scope.solvedProblems = arr;
      renderLatex();
      console.log(arr);
    });

  $scope.updateProfile = ()=> {
         $scope.body = {
            Nombre: $scope.name,
            Apellido_P: $scope.apellidoP,
            Apellido_M: $scope.apellidoM,
            Escuela: $scope.escuela,
            Pais: $scope.pais,
            Estado: $scope.estado
            // email: "dmc@gmail.com",
            // Nombre: "John",
            // Apellido_P: "Doe",
            // Apellido_M: "Coronado",
            // Pais: "Russia",
            // Estado: "Reijavik"
         }

        $http.post('/updateProfile',  $scope.body, {headers: { authorization: token}} )
        .then(function(response){
            $scope.user = response.data;
            console.log(response.data);
            $window.location.reload();
            Materialize.toast('Datos actualizados correctamente.', 4000);
        }, function(error){
            // if(error.data === "Unauthorized"){
            //     $location.path('/');
            // }
            console.log(error);
        })
    };
});
