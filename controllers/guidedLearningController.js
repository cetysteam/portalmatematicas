myApp.controller('guidedLearningController', function($scope,$http){
    var token = localStorage.getItem('token');
    $scope.problem = {};

    var renderLatex = () => {
        $('.latex').ready(function () {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, "latex"]);
        });
    }

    $http.get('/theory',{headers: {authorization:token}}).
    then(function(response){
        $scope.problem = response.data[0];
        renderLatex();
        console.log(response);
    },function(err){
        if(err.data === "Unauthorized"){
            $location.path('/');
        }
        console.log(err);
    })
});
