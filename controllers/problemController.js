myApp.controller('problemController', function ($scope, $http , $rootScope) {
    var token = localStorage.getItem('token');

    $http.get('/problem/' + sessionStorage.getItem('problemId'), { headers: { authorization: token } }).
        then(function (response) {
            $scope.problem = response.data[0];
    });

    $scope.upload = function() {
      var res = $scope.answer;
      //Send answer to db
      var body = {
        answer : res,
        problem : $scope.problem._id
      };
      var token = localStorage.getItem('token');

      $http.post('/api/uploadAnswer', body, {headers:{authorization: token}}).then(function (response){
        console.log(response);
        alert('Se ha subido correctamente');
        $scope.answer = ""; //Limpia respuesta

      }, function (error){
          console.log(error);
      });

    }

});
