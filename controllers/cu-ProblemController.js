myApp.controller('cu-ProblemController',function($scope,$http, $location){
    var token = localStorage.getItem('token');

    $scope.createProblem = () =>{
        $scope.body = {
                area : $scope.area,
                tema : $scope.topic,
                nivel : Number($scope.level),
                pregunta : $scope.problem,
                imagen : $scope.image,
                solución : $scope.solution,
                tip : $scope.tip,
                origen : $scope.origin,
                comentario : $scope.comment,
                puntaje : $scope.points
        }
        console.log($scope.body);

        $http.post('/api/cu-problem/', $scope.body ,{headers: { authorization: token }}).
        then(function (success) {
            alert("Se ha subido existosamente");
            localStorage.setItem('data',null);
            $location.path('addproblem');
        }, function (error) {
            if(error.data === "Unauthorized"){
                $location.path('/');
            }
            console.log(error);
        });
    };
});