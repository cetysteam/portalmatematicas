myApp.controller('loginController', function ($scope, $location, $http) {
    localStorage.clear();
    $scope.login = () => {
        var name = $scope.username;
        var password = $scope.password;
        $scope.body = {
            email: name,
            password: password
        }

        if ($scope.username != "" && $scope.password != "") {
            $http.post('/signin', $scope.body).
                then(function (response) {
                    var token = response.data.token; //Con este token identificar role
                    localStorage.setItem('token', token);
                    localStorage.setItem('user',$scope.email);

                    $http.get('/getRole', {headers:{authorization: token}}).then(function(success){
                      if(success.data.msg == 'admin'){
                        $location.path('homeAdmin');
                      }else{
                        $location.path('home');
                      }
                     });

                }, function (error) {
                    console.log(error);
                    alert("No users were found.");
                });
        }
        else {
            alert("No existe cuenta con ese correo.");
        }
    }
});
