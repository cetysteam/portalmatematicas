myApp.controller('uploadTheoryController', function($scope, $location, $http){
    $scope.upload = () => {
      //Falta validar que no esten vacios
      var contenido = $scope.raw;
      var problemas = $scope.rawProblema;
      var ejercicios = $scope.rawEjercicios;
      var tema = $scope.tema;
      var subtema = $scope.subtema;
      var area = $scope.area;
      var pro = $scope.pro;
      var beg = $scope.beg;
      var body = {content: contenido,
                  subject: tema,
                  subsubject: subtema,
                  area: area,
                  problema:problemas,
                  ejercicios:ejercicios,
                  pro:pro,
                beg: beg};
      //Enviar contenido a server

      let token = localStorage.getItem('token');
      $http.post('/uploadTheory', body, { headers: { authorization: token } }).then(function (response){
        console.log(response);
        alert('Se ha subido correctamente');
        $scope.raw = "";
        $scope.tema = "";
        $scope.subtema = "";
        $scope.area = "";
      }, function (error){
          console.log(error);
      });
    }
  });
