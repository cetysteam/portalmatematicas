myApp.controller('TeacherProblemsController', function($scope,$http,$rootScope,$location){
    $scope.problems = [];

    var renderLatex = ()=>{
        $('tbody').ready(function () {
            MathJax.Hub.Queue(["Typeset",MathJax.Hub, "latex"]);
        });
    }

    $scope.page = 1;
    // localStorage.clear();
    var token = localStorage.getItem('token');

    $http.get('/api/problems?page=' + $scope.page, { headers: { authorization: token } })
        .then(function (response){
                $scope.problems = response.data;
                renderLatex();

                // console.log(response.data);
            },function (error){
                if(error.data === "Unauthorized"){
                    $location.path('/');
                }
                //console.log(error);
            }
        );
    $scope.changePage = (direction) => {
        if (direction == 'next'){
            $scope.page += 1;

        }
        else if (direction == 'previous'){
            $scope.page -= 1;

        }
        $http.get('/api/problems?page=' + $scope.page, { headers: { authorization: token } })
            .then(function (response) {
                $scope.problems = response.data;
                renderLatex();
            }, function (error) {
                console.log(error);
                if(error.data === "Unauthorized"){
                    $location.path('/');
                }
            }  );

        if($scope.page > 1){
            $("#previous-button").removeClass("disabled");
            console.log("Next");
        }
        else{
            $("#previous-button").addClass("disabled");
        }
    };
    $scope.clicked = function (_id) {
        // localStorage.setItem('data', _id);
        
        sessionStorage.setItem('teacherProblemId',_id);
        $location.path('problem');
    };

    $scope.addProblem = () =>{
        sessionStorage.setItem('teacherProbblemId', null);
        $location.path('addproblem');

    }
});
