var mongoose = require('mongoose')

var teacherSchema = mongoose.Schema({
    user: mongoose.Schema.Types.ObjectId,
    nombre : String,
    apellidos : {
        paterno : String,
        materno : String
    },
    estado : String,
    escuela : String
});

module.exports = mongoose.model("Teacher", teacherSchema, "Teacher");