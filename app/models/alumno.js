var mongoose = require('mongoose')

var answerSchema = mongoose.Schema({
    problem_id: mongoose.Schema.Types.ObjectId,
    status: String,
    teacher: mongoose.Schema.Types.ObjectId,
    answer: {
        type: Array, "default": []
    },
    answerMedia: {
        type: Array, "default": []
    },
    score: Number
})

var alumnoSchema = mongoose.Schema({
    user: mongoose.Schema.Types.ObjectId,
    nombre : String,
    apellidos : {
        paterno : String,
        materno : String
    },
    estado : String,
    escuela : String,
    answers: [answerSchema]
});

module.exports = mongoose.model("Alumno", alumnoSchema, "Alumno");