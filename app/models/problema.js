var mongoose = require('mongoose')

var problemasSchema = mongoose.Schema ({
    area : String,
    tema : String,
    nivel : Number,
    pregunta : String,
    imagen : String,
    solución : String,
    tip : String,
    origen : String,
    siguente : String,
    anterior : String,
    comentario : String,
    puntaje : String
});

module.exports = mongoose.model("Problema", problemasSchema, "Problemas");
