var mongoose = require('mongoose')

var teoriaSchema = mongoose.Schema({
    area: String,
    tema: String,
    subtema: String,
    imagen: String,
    contenido: String,
    ejercicios: String,
    adicionales: String,
    index_pro: Number, //El numero de tema dentro de su area
    index_begginer: Number //El numero de problema en el orden de principiante
})


module.exports = mongoose.model("Teoria", teoriaSchema, "Teoria");
