var Problema    = require('./models/problema');
var Teoria      = require('./models/theory');
var path        = require('path');
var User        = require('./models/user');
var Estado      = require('./models/estado');
var Alumno      = require('./models/alumno');
var Teacher     = require('./models/teacher');
var Pais        = require('./models/pais');
var Preguntas   = require('./models/preguntas');
var jwt         = require('jsonwebtoken');
var config      = require('../config/db');
var passport    = require('passport');
var multer      = require('multer');
var mongoose    = require('mongoose');
var mime        = require('mime');
var fs          = require('fs');
require('../config/passport')(passport);
var upload      = multer({ dest : './data/uploads' });


var globalEmail = '';

module.exports = function (app) {
    //Middleware for access control
    function minRole(role) {
        return function(req, res, next) {
            var token = getToken(req.headers);
            var user = jwt.decode(token);
            if (user.role != role) {
                res.status(403).send({ success: false, msg: 'Role \'' + user.role + '\' is not authorized to do the requested action' });
            }
            else {
                next();
            }
        }
    }

    //Function for parsing tokens out of headers
    getToken = function(headers) {
        if (headers && headers.authorization) {
            var parted = headers.authorization.split(' ');
            if (parted.length === 2) {
                return parted[1];
            } else {
                return null;
            }
        } else {
            return null;
        }
    };

    //API Routes
    app.get("/api/problems", passport.authenticate('jwt', { session: false }), function(req,res) {
        var token = getToken(req.headers);
        if (token) {
            var decodedToken = jwt.decode(token);
            console.log(decodedToken);
            var filters = {};
            if (req.query.area) {
                filters.area = req.query.area;
            }
            if (req.query.tema) {
                filters.tema = req.query.tema;
            }
            if (req.query.nivel) {
                filters.nivel = req.query.nivel;
            }
            Problema.find(filters, function(err, problemas) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(req.query.qnty)
                    console.log(req.query.page)
                    if (req.query.qnty != null) {
                        var pageSize = req.query.qnty;
                    } else {
                        var pageSize = 20;
                    }
                    if (req.query.page == null) {
                        console.log('returning all')
                        return res.json(problemas);
                    }
                    var start = (req.query.page - 1) * pageSize;
                    var end = start * 1 + pageSize * 1 + 1 * 1;
                    console.log('returning a few')
                    return res.json(problemas.slice(start, end));
                }
            });
        } else {
            return res.status(403).send({success: false, msg: 'Unauthorized.'});
        }
    });

    app.get("/problem/:id",passport.authenticate('jwt', {session: false}),function(req,res){
        var token = getToken(req.headers);
        if(token){
            var decodedToken = jwt.decode(token);
            Problema.find({_id: req.params.id},function(err,problema){
                if(err){
                    console.log(err);
                }
                else{
                    res.send(problema);
                }
            })
        }else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    }
    );

    //TODO: Agregar soporte para archivos en la respuesta y sobreescribir una respuesta si es del mismo problema
    app.post('/api/uploadAnswer', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers)
        if (token) {
            var user = jwt.decode(token);
            if (user.role == 'alumno') {
                Alumno.findOne({ user: user._id }, function(err, userProfile) {
                    userProfile.answers.push({
                        problem_id: mongoose.Types.ObjectId(req.body.problem),
                        status: "Pending",
                        teacher: null,
                        answer: [req.body.answer],
                        score: 0
                    })
                    userProfile.save(function(err) {
                        if (err) {
                            return res.json({success: false, msg: 'Something went wrong!'})
                        }
                        return res.json({success: true, msg: 'Answer received'})
                    })
                });
            }
        } else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })

    app.post('/api/uploadAnswerMedia', passport.authenticate('jwt', {session: false}), upload.array('answerMedia'), function(req, res) {
        var token = getToken(req.headers)
        if (token) {
            var user = jwt.decode(token);

            //Save the files to a local folder
            var fileNames = []
            if(req.files) {
                console.log('getting filenames')
                req.files.forEach(function (file) {
                    var filename = file.filename + '.' + mime.getExtension(file.mimetype)
                    var promise = new Promise( function(resolve, reject) {
                        fs.rename(file.path, file.path + '.' + mime.getExtension(file.mimetype), function(err, data) {
                            if (err) {
                                reject(err)
                                return res.json({success: false, msg: 'Something went wrong saving your files'})
                            }
                            resolve()
                        })
                    })

                    promise.then( function(values) {
                        fileNames.push(filename)
                    })
                })
            }
            else {
                return res.json({success: false, msg: 'No files received'})
            }

            if (user.role == 'alumno') {
                Alumno.findOne({ user: user._id }, function(err, userProfile) {
                    console.log('found student')
                    userProfile.answers.push({
                        problem_id: mongoose.Types.ObjectId(req.body.problem),
                        status: "Pending",
                        teacher: null,
                        answer: [req.body.answer],
                        answerMedia: fileNames,
                        score: 0
                    })
                    userProfile.save(function(err) {
                        if (err) {
                            return res.json({success: false, msg: 'Something went wrong!'})
                        }
                        console.log('saved answer')
                        return res.json({success: true, msg: 'Answer received'})
                    })
                });
            }
        } else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })

    app.get('/getFile', passport.authenticate('jwt', {session: false}), function(req, res) {
        if (req.query.filename) {
            var dataPath = path.resolve(__dirname, '../data/uploads')
            if (fs.existsSync(dataPath + '/' + req.query.filename)) {
                return res.sendFile(dataPath + '/' + req.query.filename)
            }
            else {
                return res.json({success: false, msg: 'No such file'})
            }
        }
        else {
            return res.json({success: false, msg: 'No filename given'})
        }
    })

    app.get('/api/getAnswer', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers)
        if (token) {
            var user = jwt.decode(token)
            var result = {};
            var query = {}
            if (user.role == 'alumno') { //Si es un alumno
                query.user = user._id;
            } else { //Si es un profesor (requiere parametro id alumno)
                query.user = req.query.student;
            }

            //Buscar respuestas del alumno
            Alumno.findOne(query, function(err, userProfile) {
                    if (err) {
                        return res.json({success: false, msg: 'Something went wrong!'});
                    }
                    var answers = userProfile.answers.toObject()
                    result.answer = answers.find(x => x.problem_id == req.query.problem)

                    //Buscar el problema correspondiente
                    Problema.findOne({_id: mongoose.Types.ObjectId(req.query.problem)}, function(err, problem){
                        if (err) {
                            return res.json({success: false, msg: 'Something went wrong!'});
                        }
                        result.problem = problem;
                        return res.json(result)
                    })
            })
        } else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })

    app.get('/api/getAnswers', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers)
        if (token) {
            var user = jwt.decode(token)
            var result = [];
            var query = {};
            if (user.role == 'alumno') { //Si es un alumno
                query.user = user._id;
            } else { //Si es un profesor (requiere parametro id alumno)
                query.user = mongoose.Types.ObjectId(req.query.student);
            }

            //Buscar problemas resueltos por el alumno
            Alumno.findOne(query, function(err, userProfile) {
                if (err) {
                    return res.json({success: false, msg: 'Something went wrong!'});
                }
                console.log(userProfile);
                if (userProfile == null) {
                    return res.json({success: false, msg: 'Couldn\'t find user!'});
                }
                if (userProfile.answers == null) {
                    return res.json({success: false, msg: 'Couldn\'t find any answers for that user!'});
                }
                result = userProfile.answers.toObject()
                if (req.query.pending) {
                    result = result.filter(function (value) {
                        return value.status == 'Pending'
                    })
                }

                //Buscar los problemas correspondientes
                var promises = []
                result.forEach(function (answer) {
                    promises.push(new Promise(function(resolve, reject) {
                        Problema.findOne({_id: answer.problem_id}, function(err, problem){
                            if (err) {
                                reject(err)
                            }
                            answer["problem"] = problem;
                            resolve()
                        })
                    }))
                })
                console.log(promises)

                //Esperar a que la consulta termine para enviar el resultado
                Promise.all(promises).then(function(values){
                    console.log(promises)
                    console.log(result)
                    return res.json(result)
                })
            })
        } else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })

    app.get('/api/getAllAnswers', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers);
        if (token) {
            var user = jwt.decode(token);
            var result = [];
            if (user.role == 'alumno') { //Si es un alumno
                return res.json({success: false, msg: 'You need to be a teacher'})
            }
            //Buscar problemas resueltos por el alumno
            Alumno.find({}, function(err, userProfiles) {
                if (err) {
                    return res.json({success: false, msg: 'Something went wrong!'});
                }

                result = userProfiles
                result = result.filter(function(value) {
                    return value.answers.length != 0
                })

                result.forEach(function (userProfile) {
                    console.log(userProfile);
                    if (req.query.pending) {
                        userProfile.answers = userProfile.answers.toObject();
                        userProfile.answers = userProfile.answers.filter(function (value) {
                            return value.status == 'Pending'
                        })
                    }
                })
                return res.json(result)
            })
        } else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })

    app.get('/api/getScore', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers)
        if (token) {
            var user = jwt.decode(token)
            var result = [];
            var query = {};
            if (user.role == 'alumno') { //Si es un alumno
                query.user = user._id;
            } else { //Si es un profesor (requiere parametro id alumno)
                if (req.query.student == null) {
                    return res.json({success: false, msg: 'student id required as parameter'})
                }
            }

            if (req.query.student != null) { //Seleccionar alumno con base al parametro student, si es enviado
                query.user = mongoose.Types.ObjectId(req.query.student);
            }

            //Buscar problemas resueltos por el alumno
            Alumno.findOne(query, function(err, userProfile) {
                var totalScore = 0;
                if (err) {
                    return res.json({success: false, msg: 'Something went wrong!'});
                }
                console.log(userProfile);
                if (userProfile == null) {
                    return res.json({success: false, msg: 'Couldn\'t find user!'});
                }
                if (userProfile.answers == null) {
                    return res.json({success: true, score: totalScore});
                }
                result = userProfile.answers.toObject()

                //Sumar los puntajes correspondientes
                result.forEach(function (answer) {
                    if (answer.score != null) {
                        totalScore += answer.score;
                    }
                })
                return res.json({success: true, score: totalScore});
            })
        } else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })

    app.post("/api/cu-problem/",passport.authenticate('jwt', {session: false}),function(req,res){
        var token = getToken(req.headers);
        if(token){
            var decodedToken = jwt.decode(token);
            var newProblem = new Problema({
                area : req.body.area,
                tema : req.body.tema,
                nivel : req.body.nivel,
                pregunta : req.body.pregunta,
                imagen : req.body.imagen,
                solución : req.body.solucion,
                tip : req.body.tip,
                origen : req.body.origen,
                comentario : req.body.comentario,
                puntaje : req.body.puntaje
            });
            newProblem.save(function(err) {
                if (err) {
                  return res.json({success: false, msg: 'Problem already exists!'});
                }
                res.json({success: true, msg: 'Problem Registered.'});
              });

        }else{
            return res.status(403).send({success: false, msg: 'Unauthorized'});
            }
        }
    );

    app.get('/getEstados', function(req, res){

      Estado.find({}, function(err, estados){
        if(err) throw err;
        if(!estados) res.status(401).send({success: false, msg: 'Authentication failed. Estados not found.'});
        else{
          res.json(estados);
        }
      });


    });


    app.post('/getCountries', function(req, res){
      Pais.find({}, function(err, countries){
        if(err) throw err;
        if(!countries){
          res.status(401).send({success: false, msg: 'Authentication failed. Countries not found.'});

        }else{
          res.json(countries);

        }
      });
    });

    //Login and Signup routes
    app.post('/signup', function(req, res) {
        var role = 'alumno';
        if (req.body.role == 'maestro') {
            role = 'maestro pending';
        }

        if (!req.body.email || !req.body.password) {
          res.json({success: false, msg: 'Please pass email and password.'});
        } else {
          var newUser = new User({
            email: req.body.email,
            password: req.body.password,
            role: role
          });
          // save the user
          newUser.save(function(err) {
            if (err) {
              return res.json({success: false, msg: 'Email already exists.'});
            }
            if (newUser.role == "alumno") {
                console.log('Guardando como alumno')
                var newAlumno = new Alumno({
                    user : newUser._id,
                    nombre : req.body.Nombre,
                    apellidos: {
                        paterno: req.body.Apellido_P,
                        materno: req.body.Apellido_M
                    },
                    estado : req.body.Estado,
                    escuela : req.body.Escuela
                });
                newAlumno.save(function(err) {
                    if (err) {
                        User.deleteOne({email : req.body.email});
                        return res.json({success: false, msg: 'Could not create user'})
                    }
                    console.log(newUser);
                    res.json({success: true, msg: 'Successful created new user.'});
                })
            } else if (req.body.role === "maestro") {
                console.log('Guardando como maestro');
                var newTeacher = new Teacher({
                    user : newUser._id,
                    nombre : req.body.Nombre,
                    apellidos: {
                        paterno: req.body.Apellido_P,
                        materno: req.body.Apellido_M
                    },
                    estado : req.body.Estado,
                    escuela : req.body.Escuela
                });
                newTeacher.save(function(err) {
                    if (err) {
                        User.deleteOne({email : req.body.email});
                        return res.json({success: false, msg: 'Could not create user'});
                    }
                    console.log(newUser);
                    res.json({success: true, msg: 'Successful created new user.'});
                })
            }
          });
        }
    });
    //Le falta autentificar que sea admin para hacer esto
    app.post('/uploadTheory', passport.authenticate('jwt', {session: false}), minRole('admin'), function(req, res) {
      var Buffer = require('buffer').Buffer;
      var p_content = Buffer.from(req.body.content,'utf8').toString('hex');
      var p_problema = Buffer.from(req.body.problema,'utf8').toString('hex');
      var p_ejercicios = Buffer.from(req.body.ejercicios,'utf8').toString('hex');
      var t = new Teoria({
        contenido: p_content,
        area: req.body.area,
        tema: req.body.subject,
        subtema: req.body.subsubject,
        ejercicios: p_ejercicios,
        adicionales: p_problema,
        index_begginer: req.body.beg,
        index_pro: req.body.pro
      });
      t.save();//Verificar si hay error
      res.json({success:true, msg: p_content});
    });


    app.get('/getProfile', passport.authenticate('jwt', {session: false}), function(req, res){
        var token = getToken(req.headers);
        if(token) {
            var decodedToken = jwt.decode(token);
            if (decodedToken.role == 'alumno') {
                Alumno.findOne({ user: decodedToken._id }, {answers: 0}, function(err, user) {
                    return res.json(user);
                });
            } else {
                Teacher.findOne({ user: decodedToken._id }, function(err, user) {
                    return res.json(user);
                })
            }
        } else {
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    });

    //TODO: Cambiar email y modificar perfil de profesor
    app.post('/updateProfile', passport.authenticate('jwt', {session: false}), function(req, res){
        var token = getToken(req.headers)
        if (token) {
            var user = jwt.decode(token)

            if (user.role === "alumno") {

                Alumno.findOne({user: user._id}, function(err, profile) {
                    if (err) {
                        return res.json({success: false, msg: 'Something failed'})
                    }
                    //Cambiar valores
                    if (req.body.Nombre) {
                        profile.nombre = req.body.Nombre;
                    } if (req.body.Apellido_P) {
                        profile.apellidos.paterno = req.body.Apellido_P
                    } if (req.body.Apellido_M) {
                        profile.apellidos.materno = req.body.Apellido_M
                    } if (req.body.Estado) {
                        profile.estado = req.body.Estado
                    } if (req.body.Escuela) {
                        profile.escuela = req.body.Escuela
                    }
                    //Guardar
                    profile.save(function(err) {
                        //console.log("----> "+profile)
                        if (err) {
                            return res.json({success: false, msg: 'Something failed'})
                        }
                        return res.json({success: true, msg: 'Updated correctly'})
                    })
                })
            }

            else {
                //Buscar en maestros
            }
        } else {
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    })


    app.post('/signin', function(req, res) {
        User.findOne({
          email: req.body.email
        }, function(err, user) {
          if (err) throw err;

          if (!user) {
            res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
          } else {
            // check if password matches
            user.comparePassword(req.body.password, function (err, isMatch) {
              if (isMatch && !err) {
                // if user is found and password is right create a token
                var token = jwt.sign(user.toJSON(), config.secret);
                //Guarda en global para identificarlo
                globalEmail = req.body.email;
                if (user.role == 'maestro pending') {
                    return res.json({success: false, msg: 'You have not been authorized by the administrator as a teacher'})
                }
                // return the information including token as JSON
                res.json({success: true, token: 'JWT ' + token});
              } else {
                res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
              }
            });
          }
        });
    });


    app.get('/getRole', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers);
        if(token){
            var decodedToken = jwt.decode(token);
            res.json({success: true, msg: decodedToken.role});
        } else {
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    });

    app.post('/authorizeTeacher', passport.authenticate('jwt', {session: false}), minRole('admin'), function (req, res) {
        var userId = mongoose.Types.ObjectId(req.body.user)
        User.findOne({role: 'maestro pending', _id: userId}, function (err, user) {
            user.role = 'maestro'
            user.save(function(err){
                //console.log("----> "+profile)
                if (err) {
                    return res.json({success: false, msg: 'Something failed'})
                }
                return res.json({success: true, msg: 'Updated correctly'})
            })
        })
    })

    app.get('/listUsers', passport.authenticate('jwt', {session: false}), minRole('admin'), function(req, res) {
        var query = {}
        if (req.query.role) {
            query.role = req.query.role
        }
        User.find(query, function(err, users) {
            if (err) {
                return res.json({success: false, msg: 'Error retrieving users'})
            }
            else {
                return res.json(users)
            }
        })
    });


    //Requiere parametro con el numero de problema (num)
    app.get('/api/getTheory', passport.authenticate('jwt', {session: false}), function(req, res) {
        var token = getToken(req.headers);
        if (token) {
            //Principiantes; enviar el parametro solo si se desea recorrer en ese orden
            if (req.query.begginer) {
                Teoria.find({index_begginer: req.query.num}, function(err, teoriaProblemas) {
                    if (err){
                        console.log(err);
                    }
                    else {
                        var Buffer = require('buffer').Buffer;
                        if(teoriaProblemas.contenido != undefined) teoriaProblemas.contenido = Buffer.from(teoriaProblemas.contenido, 'hex').toString('utf8');
                        if(teoriaProblemas.ejercicios != undefined) teoriaProblemas.ejercicios = Buffer.from(teoriaProblemas.ejercicios, 'hex').toString('utf8');
                        if(teoriaProblemas.adicionales != undefined) teoriaProblemas.adicionales = Buffer.from(teoriaProblemas.adicionales, 'hex').toString('utf8');
                        return res.json(teoriaProblemas);
                    }
                })
            }

            //Si no es recorrido de principiantes, se requiere tambien especificar area
            else {
                Teoria.find({area: req.query.area, index_pro: req.query.num}, function(err, teoriaProblemas) {
                    if (err){
                        console.log(err);
                    }
                    else {
                        var Buffer = require('buffer').Buffer;
                        if(teoriaProblemas.contenido != undefined) teoriaProblemas.contenido = Buffer.from(teoriaProblemas.contenido, 'hex').toString('utf8');
                        if(teoriaProblemas.ejercicios != undefined) teoriaProblemas.ejercicios = Buffer.from(teoriaProblemas.ejercicios, 'hex').toString('utf8');
                        if(teoriaProblemas.adicionales != undefined) teoriaProblemas.adicionales = Buffer.from(teoriaProblemas.adicionales, 'hex').toString('utf8');
                        return res.json(teoriaProblemas);
                    }
                })
            }
        } else {
            return res.status(403).send({success: false, msg: 'Unauthorized'});
        }
    });

    app.get('/theory', passport.authenticate('jwt',{session: false}), function(req,res){
        var token = getToken(req.headers);
        if(token){
            var decodedToken = jwt.decode(token);
            Teoria.find({}, function(err, teoriaProblemas) {
                if (err) {
                    console.log(err);
                } else {
                  //Decoder
                  var Buffer = require('buffer').Buffer;
                    for(var i = 0;i < teoriaProblemas.length;i++){
                      if(teoriaProblemas[i].contenido != undefined) teoriaProblemas[i].contenido = Buffer.from(teoriaProblemas[i].contenido, 'hex').toString('utf8');
                      if(teoriaProblemas[i].ejercicios != undefined) teoriaProblemas[i].ejercicios = Buffer.from(teoriaProblemas[i].ejercicios, 'hex').toString('utf8');
                      if(teoriaProblemas[i].adicionales != undefined) teoriaProblemas[i].adicionales = Buffer.from(teoriaProblemas[i].adicionales, 'hex').toString('utf8');
                    }
                    res.json(teoriaProblemas);
                }
            });

        }
    });


    //Redirect everything else to Angular
    app.get('*', function(req, res) {
        res.sendFile(path.resolve('index.html'));
    });
};
