$('.container').ready(function () {
    MathJax.Callback.Delay(500, ["Typeset", MathJax.Hub, "latex"])
});

$("#raw-answer").keyup(function(key){
    // Evaluate if the arrow keys are not being pressed
    if(key.which < 37 || key.which > 40){
        $("#rendered-answer").html($('#raw-answer').val());
        MathJax.Hub.Typeset("rendered-answer");
    }
      
});