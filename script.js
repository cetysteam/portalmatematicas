var myApp = angular.module('Ommbc', ['ngRoute', 'routeStyles']);

myApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/login.html',
        controller: 'loginController',
        css: 'views/assets/css/login.css'
    })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'loginController',
            css: 'views/assets/css/login.css'
        })
        .when('/home', {
            templateUrl: 'views/home.html',
            controller: 'homeController',
            css: 'views/assets/css/home.css'
        }).when('/homeAdmin', {
            templateUrl: 'views/homeAdmin.html',
            controller: 'homeAdminController',
            css: 'views/assets/css/home.css'
        })
        .when('/signup', {
            templateUrl: 'views/signup.html',
            controller: "registerController",
            css: 'views/assets/css/signup.css'
        })
        .when('/problems', {
            templateUrl: 'views/search-problems.html',
            controller: 'problemsController',
            css: 'views/assets/css/search-problems.css'
        })
        .when('/problem', {
            templateUrl: 'views/problem.html',
            controller: 'problemController',
            css: 'views/assets/css/problem.css'
        })
        .when('/acceptTeachers', {
            templateUrl: 'views/acceptTeachers.html',
            controller: 'acceptTeachersController',
            css: 'views/assets/css/problem.css'
        })
        .when('/addproblem', {
            templateUrl: 'views/cu-problem.html',
            controller: 'cu-ProblemController',
            css: 'views/assets/css/cu-problem.css'
        })
        .when('/guidedLearning', {
            templateUrl: 'views/guided-learning.html',
            controller: 'guidedLearningController',
            css: 'views/assets/css/guided-learning.css'
        })
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller: 'profileController',
            css: 'views/assets/css/profile.css'
        })
        .when('/uploadTheory', {
          templateUrl: 'views/upload-theory.html',
          controller: 'uploadTheoryController',
          css: 'views/assets/css/upload-theory.css'
        })
        .when('/teacherProblems',{
            templateUrl: 'views/search-problems-Teacher.html',
            controller: 'TeacherProblemsController',
            css: 'views/assets/css/search-problems.css'
        })
        .when('/acceptTeachers',{
            templateUrl: 'views/acceptTeachers.html',
            controller: 'acceptTeachersController',
            css: 'views/assets/css/search-problems.css'
        })
        .when('/faq',{
            templateUrl: 'views/faq.html',
            controller: 'faqController',
            css: 'views/assets/css/faq.css'
        })
        .when('/pendingProblems',{
            templateUrl: 'views/pending-problems.html',
            controller: 'pendingProblemsController',
            css: 'views/assets/css/search-problems.css'
        })
        .otherwise({
            templateUrl: 'views/error.html',
            css: 'views/assets/css/error.css'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
     });
});

myApp.filter('unique', function () {
    return function (collection, keyname) {
        var output = [], keys = [];
        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});

myApp.controller('mainController', function ($scope, $location, $http) {
    $scope.user = {};
    var token = localStorage.getItem('token');
    $http.get('/getProfile',  { headers: { authorization: token }}).then(function (response){
        console.log(response);
         $scope.user = response.data;

    }, function (error){
      if(error.data === "Unauthorized"){
          $location.path('/');
      }
      console.log(error);
  });
    $scope.changeview = function (view) {
        $location.path(view);
    };

    $scope.logout = () =>{
        localStorage.clear();
        $location.path('/');

    }

});

myApp.controller('AppController',function($scope,$location){

    $scope.changeview = function (view) {
        $location.path(view);
    };
})
