
var myApp = angular.module('Ommbc', ['ngRoute']);

//     myApp.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider){

//         $stateProvider
//             .state('login',{
//                 url:'/',
//                 templateUrl: 'views/login.html'
//             })
//             .state('signup',{
//                 url:'signup',
//                 templateUrl:'views/signup.html'
//             })
//             .state('problemas',{
//                 url: 'problemas',
//                 templateUrl: 'views/search-problems.html'
//             });

//     }
// ]);

    // configure our routes
    myApp.config(function($routeProvider) {
        $routeProvider.when('/', {
                templateUrl : 'views/login.html',
                controller  : 'loginController'
                // css: 'views/assets/css/login.css'
            })
            .when('/home', {
                templateUrl : 'views/home.html',
                controller  : 'mainController'
            })
            .when('/signup',{
                templateUrl: 'views/signup.html',
                controller : "registerController"
            })
            .when('/problemas',{
                templateUrl: 'views/search-problems.html',
                controller: 'problemsController'
                // css: 'views/assets/css/search-problems.css'
            })

            // // route for the contact page
            // .when('/contact', {
            //     templateUrl : 'pages/contact.html',
            //     controller  : 'contactController'
            // });
    });


    myApp.controller('mainController',function($scope,$location) {
        
        $scope.changeview = function(view) {
            $location.path(view);
        }
    });

    myApp.controller('loginController', function($scope,$location) {

        $scope.username = "";
        $scope.password = "";
        $scope.cambiar = () =>{
            if($scope.username  != "" && $scope.password != ""){
                $location.path('/problemas');

            }
            else{
                alert("Faltan campos por llenar");
            }
        } 
    });

    myApp.controller('registerController', function($scope,$location) {
        $scope.changeview = function(view){
            $location.path(view);
        }
    });

    myApp.controller('problemsController', function($scope, $http){

        // $scope.problemas = [{
        //     tema:' OMMBC'
        // }]; // <-array
        $http.get('/problemas').
            then(function(response) {
                $scope.problemas = response.data;
                // console.log(response.data.tema);
            });
    

        //     $scope.myArray = [];
        // $http.get("http://localhost:8080/xyz").then(function (response) {
        //     $scope.myArray = response;
        // });
        
    })